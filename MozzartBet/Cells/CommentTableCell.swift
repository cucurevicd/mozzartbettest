//
//  CommentTableCell.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class CommentTableCell: BaseTableViewCell {

    // MARK:- Outlets
    @IBOutlet weak var commentText: UITextView!
    @IBOutlet weak var commentMinute: UILabel!
    
    var comment: Comment?{
        
        didSet{
            
            if comment != nil{
                
                configUI()
            }
        }
    }
    

    override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
        super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)
        
        
        if let commentData = data as? Comment{
            
            self.comment = commentData
        }
    }
    
    
    func configUI(){
        
        if let text = comment!.text{
        
            self.commentText.text = text
        }

        if let minut = comment!.time{
            
            self.commentMinute.text = "\(minut)"
        }
    }
}
