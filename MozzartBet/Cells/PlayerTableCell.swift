//
//  PlayerTableCell.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class PlayerTableCell: BaseTableViewCell {

    // MARK:- Outlets
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var position: UILabel!

    var player: Player?{
        
        didSet{
            
            if player != nil{
                
                configUI()
            }
        }
    }
    
    
    override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
        super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)
        
        
        if let playerData = data as? Player{
            
            self.player = playerData
        }
    }
    
    
    func configUI(){
        
        if let name = player!.name{
            
            self.name.text = name
        }
        
        if let number = player!.number{
            
            self.number.text = "No.:\(number)"
        }
        
        if let position = player!.position{
            
            self.position.text = "Position: \(position)"
        }
    }
    
}
