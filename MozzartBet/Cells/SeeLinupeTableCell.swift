//
//  SeeLinupeTableCell.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class SeeLinupeTableCell: BaseTableViewCell {


    @IBAction func seeLineupHomeAction(_ sender: UIButton) {
        
        let nextVC = Util.VC("GameDetail", vcName: "SpecificDetailsViewController") as! SpecificDetailsViewController
        nextVC.specificDetails = .lineup1
        self.context?.goNext(nextVC)
    }
    
    @IBAction func seeLineupGuestAction(_ sender: UIButton) {
        
        let nextVC = Util.VC("GameDetail", vcName: "SpecificDetailsViewController") as! SpecificDetailsViewController
        nextVC.specificDetails = .lineup2
        self.context?.goNext(nextVC)
    }

}
