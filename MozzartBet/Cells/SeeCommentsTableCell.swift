//
//  SeeCommentsTableCell.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class SeeCommentsTableCell: BaseTableViewCell {


    override func cellIsSelected() {
        
        let nextVC = Util.VC("GameDetail", vcName: "SpecificDetailsViewController") as! SpecificDetailsViewController
        nextVC.specificDetails = .comments
        self.context?.goNext(nextVC)
    }
}
