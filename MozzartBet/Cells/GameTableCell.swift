//
//  GameTableCell.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/28/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class GameTableCell: BaseTableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var guestTeamLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var tournamentName: UILabel!
    @IBOutlet weak var currentTime: UILabel!
    
    @IBOutlet weak var halfTimeResult: UILabel!
    @IBOutlet weak var gameStatus: UILabel!
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var homeEventStack: UIStackView!
    @IBOutlet weak var guestEventStack: UIStackView!
    @IBOutlet weak var favoriteButton: UIButton!
    
    // MARK:- Properties
    var game: Game?{
        
        didSet{
            
            if game != nil{
                
                configUI()
                setHomeEvents()
                setGuestEvents()
                
                self.layoutIfNeeded()
            }
        }
    }
    

    // MARK:- Config
    override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
        super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)
        
        if let gameData = data as? Game{
            
            self.game = gameData
        }
    }

    /// Set design and data
    func configUI(){
        
        // Hide favorite if game is finished
        favoriteButton.isHidden = game?.statusCode == 100
        
        // Set teams
        if let home = game?.homeTeam{
            
            if let name = home.name{
            
                homeTeamLabel.text = name
            }
        }
        
        if let guest = game?.guestTeam{
            
            if let name = guest.name{
                
                guestTeamLabel.text = name
            }
        }
        
        // Set current result
        if let result = game?.currentTimeScore{
            
            if let homeScore = result.homeTeam, let guestScore = result.guestTeam{
             
                resultLabel.text = "\(homeScore) : \(guestScore)"
            }
        }
        
        // Set half score
        if let half = game?.halfTimeScore{
            
            if let home = half.homeTeam, let guest = half.guestTeam{
                
                halfTimeResult.text = "(\(home):\(guest))"
            }
        }
        
        // Tournament name
        if let tournament = game?.tournamentName{
            
            tournamentName.text = tournament
        }
        
        // Set time
        if let matchTime = game?.matchTime{
            
            currentTime.text = matchTime
        }
        
        
        // Set category image
        if let category = game?.categoryId{
            
            ImageDownload.getCategoryImage(categoryId: category, imageView: categoryImage)
        }
        
        // Set favorite
        if let matchId = game?.matchId{
            
            favoriteButton.isSelected = (favorite: DataController.sharedInstance.favorites!.contains(matchId))
        }
        
    }

    
    /// Set all game events
    func setHomeEvents(){
        
        // Set home events
        for view in homeEventStack.arrangedSubviews{
            view.removeFromSuperview()
        }
        var homeEvents = [AnyObject]()
        
        if let homeYellowCards = game?.homeTeamCards?.yellowCards{
            
            for card in homeYellowCards{
                homeEvents.append(card)
            }
        }
        
        if let homeRedCard = game?.homeTeamCards?.redCards{
            
            for card in homeRedCard{
                homeEvents.append(card)
            }
        }
        
        if let homeGoals = game?.goalsHome{
            
            for goal in homeGoals{
                homeEvents.append(goal)
            }
        }
        
        // Sort events
        let sort = homeEvents.sorted { left, right -> Bool in
            guard let rightKey = right["time"] as? NSInteger else { return true }
            guard let leftKey = left["time"] as? NSInteger else { return false }
            return leftKey > rightKey
        }
        
        for event in sort{
        
            let view = EventView.fromNib()
            view.configView(event: event)
            homeEventStack.addArrangedSubview(view)
        }
    }
    
    func setGuestEvents(){
        
        // Set guest events
        for view in guestEventStack.arrangedSubviews{
            view.removeFromSuperview()
        }
        var guestEvents = [AnyObject]()
        
        if let guestYellowCards = game?.guestTeamcards?.yellowCards{
            
            for card in guestYellowCards{
                guestEvents.append(card)
            }
        }
        
        if let guestRedCard = game?.guestTeamcards?.redCards{
            
            for card in guestRedCard{
                guestEvents.append(card)
            }
        }
        
        if let guestGoals = game?.goalsGuest{
            
            for goal in guestGoals{
                guestEvents.append(goal)
            }
        }
        
        // Sort events
        let sortEvents = guestEvents.sorted { left, right -> Bool in
            guard let rightKey = right["time"] as? NSInteger else { return true }
            guard let leftKey = left["time"] as? NSInteger else { return false }
            return leftKey < rightKey
        }
        
        for event in sortEvents{
            
            let view = EventView.fromNib()
            view.configView(event: event)
            guestEventStack.addArrangedSubview(view)
        }

        
    }
    
    @IBAction func favoriteAction(_ sender: UIButton) {
        
        if let id = game?.matchId{
        
            if DataController.sharedInstance.favorites!.contains(id){
                
                DataController.sharedInstance.favorites?.remove(id)
            }
            else{
                
                DataController.sharedInstance.favorites?.add(id)
            }
        }
        
        sender.isSelected = !sender.isSelected
    }
    
    // MARK:- Cell selection
    
    override func cellIsSelected() {
        
        if let gameId = game?.matchId{
            
            let nextVC = Util.VC("GameDetail", vcName: "GameDetailsViewController") as! GameDetailsViewController
            nextVC.gameId = gameId
            self.context?.goNext(nextVC)
        }
        
    }
}
