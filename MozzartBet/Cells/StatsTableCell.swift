//
//  StatsTableCell.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class StatsTableCell: BaseTableViewCell {
    
    // MARK:- Outlets
    @IBOutlet weak var halfName: UILabel!
    @IBOutlet weak var assistsFirst: UILabel!
    @IBOutlet weak var assistsSecond: UILabel!
    @IBOutlet weak var blockFirst: UILabel!
    @IBOutlet weak var blockSecond: UILabel!
    @IBOutlet weak var cornerFirst: UILabel!
    @IBOutlet weak var cornerSecond: UILabel!
    @IBOutlet weak var foulFirst: UILabel!
    @IBOutlet weak var foulSecond: UILabel!
    @IBOutlet weak var offsideFirst: UILabel!
    @IBOutlet weak var offsideSecond: UILabel!
    @IBOutlet weak var redCardsFirst: UILabel!
    @IBOutlet weak var redCardsSecond: UILabel!
    @IBOutlet weak var yellowCardFirst: UILabel!
    @IBOutlet weak var yellowcardSecond: UILabel!
    @IBOutlet weak var shotsOfTargetFirst: UILabel!
    @IBOutlet weak var shotsOfTargetSecond: UILabel!
    @IBOutlet weak var shotsOnTargetFirst: UILabel!
    @IBOutlet weak var shotsOnTargetSecond: UILabel!
    
    
    var statsFirst: Stats?{
        
        didSet{
            
            if statsFirst != nil{
                
                configFirstUI(stats: statsFirst!)
            }
        }
    }
    
    var statsSecond:Stats?{
        
        didSet{
            
            if statsSecond != nil{
                
                configSecondUI(stats: statsSecond!)
            }
        }
    }
    
    
    // MARK:- Config
    override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
        super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)
        
        
        if let statsData = data?["1"] as? Stats{
            
            self.statsFirst = statsData
        }
        
        if let statsData = data?["2"] as? Stats{
            
            self.statsSecond = statsData
        }
        
        self.halfName.text = "Stats"
    }
    
    /// Set design and data
    func configSecondUI(stats: Stats){
        
        self.assistsSecond.text = "\(stats.assists!)"
        self.blockSecond.text = "\(stats.blockedShots!)"
        self.cornerSecond.text = "\(stats.corners!)"
        self.foulSecond.text = "\(stats.corners!)"
        self.offsideSecond.text = "\(stats.offsides!)"
        self.redCardsSecond.text = "\(stats.redCards!)"
        self.yellowcardSecond.text = "\(stats.yellowCards!)"
        self.shotsOfTargetSecond.text = "\(stats.shotsOffTarget!)"
        self.shotsOnTargetSecond.text = "\(stats.shotsOnTarget!)"
    }
    
    /// Set design and data
    func configFirstUI(stats: Stats){
        
        self.assistsFirst.text = "\(stats.assists!)"
        self.blockFirst.text = "\(stats.blockedShots!)"
        self.cornerFirst.text = "\(stats.corners!)"
        self.foulFirst.text = "\(stats.corners!)"
        self.offsideFirst.text = "\(stats.offsides!)"
        self.redCardsFirst.text = "\(stats.redCards!)"
        self.yellowCardFirst.text = "\(stats.yellowCards!)"
        self.shotsOfTargetFirst.text = "\(stats.shotsOffTarget!)"
        self.shotsOnTargetFirst.text = "\(stats.shotsOnTarget!)"
    }
    
}
