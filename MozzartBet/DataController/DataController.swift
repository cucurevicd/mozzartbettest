//
//  DataController.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/27/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Alamofire

class DataController: NSObject {
    
    enum GameType: NSInteger {
        case allGames
        case favorite
        case live
        case finish
        case next
    }
    
    //MARK:- Singleton with empty array
    static let sharedInstance : DataController = {
        
        let instance = DataController(array: [])
        return instance
    }()
    
    //MARK:- Properties
    
    var syncTimeInterval : TimeInterval = 60 // Update every minute
    var lastSync: NSInteger?
    dynamic var games: [Game]?
    var favorites: NSMutableArray?
    var timer: Timer?
    var gameType: GameType?
    
    // Game details
    dynamic var detailGame: Game?
    
    var observerdGameId: NSInteger?
    
    //MARK: Init
    
    init( array : [Game]) {
        games = array
        favorites = NSMutableArray()
    }
    
    
    /// Config game for every new tab
    ///
    /// - Parameter type: Game type
    static func configDataController(type: GameType){
        
        sharedInstance.gameType =  type
        
        // Restart timer
        if sharedInstance.timer != nil{
            
            sharedInstance.timer?.invalidate()
            sharedInstance.timer = nil
        }
        
        sharedInstance.timer = Timer.scheduledTimer(timeInterval: sharedInstance.syncTimeInterval, target: self, selector: #selector(DataController.loadGames), userInfo: nil, repeats: true)
    }
    
    /// Config game detals because of timer to reloasyncd on time interval
    ///
    /// - Parameter gameId: Observe game id
    static func configGameDetailsController(gameId: NSInteger){
        
        sharedInstance.observerdGameId = gameId
        
        // Restart timer
        if sharedInstance.timer != nil{
            
            sharedInstance.timer?.invalidate()
            sharedInstance.timer = nil
        }
        
        sharedInstance.timer = Timer.scheduledTimer(timeInterval: sharedInstance.syncTimeInterval, target: self, selector: #selector(DataController.loadGameDetails), userInfo: nil, repeats: true)
    }
    
    /// Filter games based on parameter
    ///
    /// - Parameter games: All new games
    /// - Returns: Filter games
    static func filterGames(games: [Game]) -> [Game]{
        
        switch sharedInstance.gameType! {
        case .favorite:
            return games.filter{sharedInstance.favorites!.contains($0.matchId as Any)}
            
        case .finish:
            return games.filter{$0.statusCode == 100}
            
        case .next:
            return games.filter{$0.statusCode == 0}
        default:
            return games
        }
    }
    
    /// Lod games based on type and return filtered games
    ///
    /// - Parameter gamesFilter: Filtered games
    static func loadGames(){
        
        var params: Parameters?
        let startCountDay = Calendar.current.date(byAdding: .day, value: -2, to: Date())
        
        switch sharedInstance.gameType! {
        case .allGames, .favorite:
            params = [sportId : 1]
            break
        case .live:
            params = [sportId: 1, liveType: "LIVE"]
            break
        case .finish:
            params = [sportId: 1, fromTime: Int(startCountDay!.timeIntervalSince1970), untilTime: Int(Date().timeIntervalSince1970)]
            break
        case .next:
            params = [sportId: 1, fromTime: Int(Date().timeIntervalSince1970)]
            break
        }
        
        ApiModel.getGames(params!) { (resultGames) in
            
            print(filterGames(games: resultGames))
            sharedInstance.games = filterGames(games: resultGames)
        }
    }
    
    
    /// Load details of game
    ///
    /// - Parameter gameId: Details game id
    static func loadGameDetails(){
        
        ApiModel.getGameDetails(gameId: sharedInstance.observerdGameId!) { (game) in
            
            if game != nil{
                
                sharedInstance.detailGame = game
            }
            else{
                Util.alertWithTitle("Greška", message: "Trenunto nema detalja za izabranu igru")
            }
        }
    }
}
