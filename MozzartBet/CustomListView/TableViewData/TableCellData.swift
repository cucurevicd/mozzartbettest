//
//  TableCellData.swift
//  BusinessSeats
//
//  Created by Intellex on 3/21/16.
//  Copyright © 2016 Intellex. All rights reserved.
//

import UIKit

class TableCellData: NSObject {

	/// Cell reuse identifier
	var cellName: String?

	/// Data for cell
	var cellData: AnyObject?

	init(name: String, data: AnyObject?) {
		cellName = name
		cellData = data
	}
}
