//
//  TableSectionData.swift
//  
//
//  Created by Intellex on 3/21/16.
//
//

import UIKit

class TableSectionData: NSObject
{

	/// Name for header cell. If it is nil, there will be no header
	var sectionName : String?
	
	/// Data for configuration header cell
	var sectionData : AnyObject?
	
	/// Array of table cell datas. All cell in section
	var sectionCellData : [TableCellData]?

	/// Set pagination. First number is section which wanto to be paginated
	var pagination : [NSInteger :Bool]?
    
    
    /// Specific height of section
    var sectionHeight: CGFloat?
	
	/**
	Init section data

	- parameter sectionName: Name for section and header cell
	- parameter data:        Data for config header cell
	- parameter cellData:    Array of all cells

	- returns: TableSectionData
	*/
	
	init(sectionName : String?, data : AnyObject?, cellData : [TableCellData]?)
	{

		self.sectionName = sectionName
		self.sectionData = data
		self.sectionCellData = cellData
	}
	
	
}
