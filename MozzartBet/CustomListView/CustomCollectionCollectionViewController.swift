//
//  CustomCollectionViewController.swift
//  BusinessSeats
//
//  Created by Intellex on 3/23/16.
//  Copyright © 2016 Intellex. All rights reserved.
//

import UIKit



/**
*  Collection view page delegate for getting page and offset in implemented view controller
*/
protocol CollectionViewPageDelegate {

	func collectionPageSet(_ page: NSInteger)
	func collectionViewOffset(_ offset: CGFloat)
}

class CustomCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

	/// Main reuse identifier
	var reuseIdentifier = "MainCollectionCell"

	// MARK:- Properties

	/// Collection page delegate
	var pageDelegate: CollectionViewPageDelegate?

	/// Loading more enable
	var loadMoreMembers: Bool?

	/// Array of all data
	var dataArray = [AnyObject]()
    
    var cellAutolayout: Bool?


	// MARK:- View cycle

	override func viewDidLoad() {
		super.viewDidLoad()
        
        // Set autolayout cell
        if cellAutolayout != nil{
            
            if cellAutolayout!{
                
                if let cvl = collectionViewLayout as? UICollectionViewFlowLayout {
                    
                    cvl.estimatedItemSize = CGSize(width: (collectionView?.frame.width)!, height: 75)
                }
            }
        }
	}

	override func viewWillAppear(_ animated: Bool) {
		
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	// MARK: UICollectionViewDataSource

	override func numberOfSections(in collectionView: UICollectionView) -> Int {

		return 1
	}

	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

		return dataArray.count
	}

	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
		
		// Configure the cell
		(cell as? BaseCollectionViewCell)?.configCellWithdata(dataArray[(indexPath as NSIndexPath).item], context: (self.parent as? BaseViewController)!, indexPath: indexPath)

		return cell
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

		return CGSize(width: self.view.frame.width, height: self.view.frame.height)
	}

	// MARK:- Scroll view

	/**
	Use delegate method to track scrollView offset

	- parameter scrollView: CollectionView scroll
	*/
	override func scrollViewDidScroll(_ scrollView: UIScrollView) {

		self.pageDelegate?.collectionViewOffset(scrollView.contentOffset.x)
	}
    	
	/**
	Use delegate method to track collection view current page

	- parameter scrollView: CollectionView scroll
	*/
	override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

		self.pageDelegate?.collectionPageSet(NSInteger(scrollView.contentOffset.x / scrollView.frame.width))
		print("\n\nScroll offset: \(scrollView.contentOffset) \n\n")
	}
    
	
	
}
