//
//  CollectionCellWithTableView.swift
//  BusinessSeats
//
//  Created by Intellex on 3/23/16.
//  Copyright © 2016 Intellex. All rights reserved.
//

import UIKit

class CollectionCellWithTableView: BaseCollectionViewCell, UITableViewDelegate, UITableViewDataSource, TableCellDeleteDelegate {
	
	// MARK:- Properties
	
	@IBOutlet var tableView: UITableView!
	var dataArray = [TableSectionData]()
	
	// First parametar is number of section, and second is flag for loading more
	var loadingMore: [Int: Bool]?{

		didSet{

			registerCellForIdentifier("LoadingTableCell")
		}
	}

	
	// MARK:- Config
	
	override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath) {
		super.configCellWithdata(data, context: context, indexPath: indexPath)

		if data == nil{
			return
		}

		//If data contains value is loding more, handle it
		if let tableData = data as? [TableSectionData]{
			self.dataArray = tableData
		}

		self.tableView.delegate = self
		self.tableView.dataSource = self
		
		self.tableView.rowHeight = UITableViewAutomaticDimension
		self.tableView.estimatedRowHeight = 100.0
		self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
		self.tableView.estimatedSectionHeaderHeight = 60.0
		
		self.registerCells()
		
		self.tableView.reloadData()
	}
	
	// MARK: - Table view data source
	
	func numberOfSections(in tableView: UITableView) -> Int {
		
		var counter = 0
		for sectionData in self.dataArray {
			if sectionData.sectionName != nil {
				counter += 1
			}
		}
		
		return counter
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		var allRows = 0
		
		if let section = dataArray[section] as? TableSectionData {
			allRows = (section.sectionCellData?.count) ?? 0

			// Pagination set
			self.loadingMore = section.pagination
		}
		
		// Add loading cell at end of section
		if self.loadingMore != nil {
			
			if (self.loadingMore?.keys.first! == section) && (self.loadingMore!.values.first!) {
				allRows += 1
			}
		}
		
		return allRows
	}
	
	// Section header
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		
		let sectionCell = self.dataArray[section]
		
		// If there is no need for header view, we set EMPTY STRING!!
		if sectionCell.sectionName == EMPTY_STRING {
			return UIView(frame: CGRect.zero)
		}
		
		else {
			
			// Configure cell name
			let cellHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionCell.sectionName!) as? BaseHeaderFooterView
			cellHeader?.configHeaderFooterWithdata(sectionCell.sectionData, context: context!, indexPath: nil)
			return cellHeader
		}
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		let sectionCell = self.dataArray[section]
		if sectionCell.sectionName == EMPTY_STRING {
			return 0.0
		}
		
		else {
			return UITableViewAutomaticDimension
		}
	}
	
	// Cell
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		var cellName = String()
		var cellData: AnyObject?
		let sectionData = dataArray[(indexPath as NSIndexPath).section]
		
		// Config loading cell at end of section
		if self.loadingMore != nil && sectionData.sectionCellData?.count == (indexPath as NSIndexPath).row {
			
			if (self.loadingMore?.keys.first! == (indexPath as NSIndexPath).section) && (self.loadingMore!.values.first!) {
				cellName = "LoadingTableCell"
				
			}
		} else {
			
			cellData = sectionData.sectionCellData![(indexPath as NSIndexPath).row].cellData
			cellName = sectionData.sectionCellData![(indexPath as NSIndexPath).row].cellName!
		}
		
		// Configure cell name
		let cell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath)

		// Set cell delagate for loading more data
		if self.loadingMore != nil && sectionData.sectionCellData?.count == (indexPath as NSIndexPath).row {

			(cell as? LoadingTableCell)?.delegate = context
		}

		// Configure cell
		(cell as? BaseTableViewCell)?.configCellWithdata(cellData, context: context!, indexPath: indexPath, lastCell: ((sectionData.sectionCellData?.count)! - 1) == (indexPath as NSIndexPath).row, isCellSelected: nil)
		
		// Set delegate
		(cell as? BaseTableViewCell)?.tableCellSelectedDelegate = context!
		(cell as? BaseTableViewCell)?.tableCellDeleteDelegate = self
		
		cell.selectionStyle = .none
		
		return cell
	}

	//MARK:- Delete table view cell(s)

	/// Delete one cell
	///
	/// - parameter data:      Cell data
	/// - parameter indexPath: Index Path
	func deleteCell(_ data: AnyObject?, indexPath: IndexPath) {

		let section = self.dataArray[indexPath.section]
		section.sectionCellData?.remove(at: indexPath.row)

		DispatchQueue.main.async {

			self.tableView.beginUpdates()

			self.tableView.deleteRows(at: [indexPath], with: .automatic)

			self.tableView.endUpdates()
		}
	}
	
	
	
	/// Delete selveral selected cells
	///
	/// - parameter sectionNo:  Section from where to delete cells
	/// - parameter indexPaths: Selected index paths
	func deleteMultipleCells(sectionNo: NSInteger, indexPaths : [IndexPath]){

		let dataSection = dataArray[sectionNo]

		for i in indexPaths{

			dataSection.sectionCellData?.remove(at: i.row)
		}

		DispatchQueue.main.async {

			self.tableView.beginUpdates()

			self.tableView.deleteRows(at: indexPaths, with: .automatic)

			self.tableView.endUpdates()
		}
	}
	
	// MARK:- Regsister cell
	
	/**
	If using xib for presenting cell, register it here for table view
	#Important: Use for nibName and cellReuseIdentifier same string!!
	*/
	func registerCells() {
		
		for section in self.dataArray {
			
			// Header cell register
			if let sectionCellName = section.sectionName {
				
				// Empty string is for creating section with zero height
				if sectionCellName != EMPTY_STRING {
					tableView.register(UINib(nibName: sectionCellName, bundle: nil), forHeaderFooterViewReuseIdentifier: sectionCellName)
				}
			}
			
			if let sectionData = section.sectionCellData {
				
				for cell in sectionData {

					// Register cell
					if let cellIdentifier = cell.cellName {
						registerCellForIdentifier(cellIdentifier)
					}
				}
			}
		}
	}
	
	/**
	Register cell for table view

	- parameter cellIdentifier: The reuse identifier
	*/
	func registerCellForIdentifier(_ cellIdentifier: String) {
		tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
	}
	
}
