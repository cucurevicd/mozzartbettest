//
//  BaseHeaderFooterView.swift
//  Quiz4U
//
//  Created by Intellex on 10/17/16.
//  Copyright © 2016 Intellex. All rights reserved.
//

import UIKit

class BaseHeaderFooterView: UITableViewHeaderFooterView {

	// MARK:- Properties

	/// Main data of cell
	var mainData: AnyObject?

	/// Context where table view is impelemented
	var context: BaseViewController?

	/// Index path of cell
	var indexPath: IndexPath!
	

	/**
	Apstract function for header View. Cells use data which will be parsed in particullar cell.

	- parameter data:      Data to configure cell
	- parameter context:   View controller which where cell is presenting
	- parameter indexPath: Index path of cell
	*/
	func configHeaderFooterWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?) {

		/**
		*  Set all data to cell instance
		*/
		self.mainData = data
		self.context = context
		self.indexPath = indexPath
	}

}
