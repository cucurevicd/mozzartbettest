//
//  BaseTableViewCell.swift
//  EKapija
//
//  Created by Intellex on 12/23/15.
//  Copyright © 2015 Intellex. All rights reserved.
//

import UIKit

/**
*  Table cell select protocol for implementing in table view embeded context
*/
protocol TableCellSelectedProtocol {
	func tableCellSelected(_ data: AnyObject?)
	func tableCellSelectedAtIndexPath(_ data: AnyObject?, indexPath: IndexPath)

}

protocol TableCellDeleteDelegate {

	func deleteCell(_ data: AnyObject?, indexPath: IndexPath)
}

class BaseTableViewCell: UITableViewCell {

	// MARK:- Properties

	/// Main data of cell
	var mainData: AnyObject?

	/// Context where table view is impelemented
	var context: BaseViewController?
    
	/// Index path of cell
	var indexPath: IndexPath!

	/// Table cell delegate
	var tableCellSelectedDelegate: TableCellSelectedProtocol?

	/// Table cell delete delegate
	var tableCellDeleteDelegate: TableCellDeleteDelegate?

	// Flag for selected cell. If it is not nil, than cell is multiple selcted
	var cellSelected: Bool?

	// Flag for last cell in section
	var isLastCell: Bool?

	/// Separator view on bottom of cell
	@IBOutlet var separatorView: UIView!

	/// Main view of cell
	@IBOutlet weak var mainView: UIView!

	@IBOutlet weak var selectorIndicator: UIImageView!

	// MARK:- Methods

	override func awakeFromNib() {
		super.awakeFromNib()

        
	}

	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
        
		
		if selectorIndicator != nil{
			
			selectorIndicator.isHighlighted = selected
		}
		
		// Configure the view for the selected state
		if selected ||  self.cellSelected != nil{
            
			// Configure selectorIndicator
			
			self.cellIsSelected()
		}
	}
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated:animated)
        
        // Configure selectorIndicator
        if selectorIndicator != nil{
            
            selectorIndicator.isHighlighted = highlighted
        }
    }

	/**
	Apstract function for all cells. Cells use data which will be parsed in particullar cell.

	- parameter data:      Data to configure cell
	- parameter context:   View controller which where cell is presenting
	- parameter indexPath: Index path of cell
	*/
	func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected : Bool?) {

		/**
		*  Set all data to cell instance
		*/
		self.mainData = data
		self.context = context
		self.indexPath = indexPath
		self.cellSelected = isCellSelected
		self.isLastCell = lastCell

		if separatorView != nil {
			separatorView.isHidden = lastCell
		}

		self.selectionStyle = .none
	}

	/**
	Apstract function to be called when cell is selected. Also delegat method implemented
	*/
	func cellIsSelected() {
        
		/**
		*  If we need delegate for calling method for selecting cell outside the cell class
		*/
		if (tableCellSelectedDelegate != nil) {
			self.tableCellSelectedDelegate?.tableCellSelected(self.mainData)
			self.tableCellSelectedDelegate?.tableCellSelectedAtIndexPath(self.mainData, indexPath: self.indexPath!)
		}
	}

	/**
	Apstract function to be called when cell wants to be deleted.
	*/
	func deleteCell() {

		if (tableCellSelectedDelegate != nil) {

			self.tableCellDeleteDelegate?.deleteCell(self.mainData, indexPath: self.indexPath)
		}
	}

	func setCellBorderColor() {
		self.contentView.layer.borderColor = UIColor.black.cgColor
		self.contentView.layer.borderWidth = 1.5
	}
}
