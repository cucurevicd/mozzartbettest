//
//  CustomTableViewController.swift
//  BusinessSeats
//
//  Created by Intellex on 3/18/16.
//  Copyright © 2016 Intellex. All rights reserved.
//

import UIKit

/// Accordion cell type for storing accordion cell height and index path
class AccordionCellType {

	var index = IndexPath()
	var cellHeight: CGFloat = 0

	init(index: IndexPath, cellHeight: CGFloat) {

		self.cellHeight = cellHeight;
		self.index = index
	}
}

/**
*  Swipe protocol
*/
protocol SwipeActionDelegate {

	func swipeAction(_ data: AnyObject, indexPath: IndexPath, actionName: String)
}

/**
*  Protocol for selectable table view
*/
protocol SelectableTableViewDelegate {
	func selectableCellAction(_ indexPath: IndexPath)
}

class CustomTableViewController: UITableViewController, TableCellDeleteDelegate {

	// MARK: - Properties

	/// Array of data of sections
    var dataArray = [TableSectionData](){
        
        didSet{
            
            registerCells()
        }
    }

	/// First parametar is number of section, and second is flag for loading more
	var loadingMore: [Int: Bool]?

	/// Edit of table view
	var editEnabled = false

	/// Actions names enabled on edit of cell
	var editActions = [String]()

	/// Swipe cell delegate
	var swipeDelegate: SwipeActionDelegate?

	/// Selectable table view delegate
	var selectableTableDelegate : SelectableTableViewDelegate?

	/// Accordion enabled
	var accordion: Bool?

	/// Array of accordion data (height and index path)
	var accordionCellsData = [AccordionCellType]()
    
    var explicitRowheight: CGFloat?

	// MARK: - View cycle

	override func viewDidLoad() {
		super.viewDidLoad()
        
		// Table view height
        if explicitRowheight == nil {
		self.tableView.rowHeight = UITableViewAutomaticDimension
		self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension

		self.tableView.estimatedRowHeight = 140.0
		self.tableView.estimatedSectionHeaderHeight = 60.0
        }
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	// MARK: - Table view data source

	override func numberOfSections(in tableView: UITableView) -> Int {

		return dataArray.count
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		var allRows = 0

		if let section = dataArray[section] as? TableSectionData {
			allRows = (section.sectionCellData?.count) ?? 0
		}

		// Add loading cell at end of section
		if self.loadingMore != nil {

			if (self.loadingMore?.keys.first! == section) && (self.loadingMore!.values.first!) {
				allRows += 1
			}
		}

		return allRows
	}

	/**
	Table view header

	- parameter tableView: Table view
	- parameter section:   Selected section

	- returns: UIView
	*/
	override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

		let sectionCell = self.dataArray[section]

		// If there is no need for header view, we set section name as nil
		if sectionCell.sectionName == nil {
			self.tableView.sectionHeaderHeight = 0
			return UIView(frame: CGRect.zero)
		}

		else {

			// Configure cell name
			let cellHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: sectionCell.sectionName!) as? BaseHeaderFooterView
			cellHeader?.configHeaderFooterWithdata(sectionCell.sectionData, context: UIApplication.topViewController() as! BaseViewController?, indexPath: nil)
			self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
			
			return cellHeader
		}
	}

	/**
	Config cell

	- parameter tableView: Table view
	- parameter indexPath: Index Path

	- returns: UITableViewCell
	*/
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		var cellName = String()
		var cellData: AnyObject?
		let sectionData = dataArray[(indexPath as NSIndexPath).section]

		// Config loading cell at end of section
		if self.loadingMore != nil && sectionData.sectionCellData?.count == (indexPath as NSIndexPath).row {

			if (self.loadingMore?.keys.first! == (indexPath as NSIndexPath).section) && (self.loadingMore!.values.first!) {
				cellName = "LoadingTableCell"
			}
		} else {

			cellData = sectionData.sectionCellData![(indexPath as NSIndexPath).row].cellData
			cellName = sectionData.sectionCellData![(indexPath as NSIndexPath).row].cellName!
		}

		// Configure cell name
		let cell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath)

		// Set cell delagate for loading more data
		if self.loadingMore != nil && sectionData.sectionCellData?.count == (indexPath as NSIndexPath).row {

			(cell as? LoadingTableCell)?.delegate = self.parent as? BaseViewController
		}

		// Configure cell
		(cell as? BaseTableViewCell)?.configCellWithdata(cellData, context: (self.parent as? BaseViewController)!, indexPath: indexPath, lastCell: ((sectionData.sectionCellData?.count)! - 1) == (indexPath as NSIndexPath).row, isCellSelected: nil)

		// Set delegate
		(cell as? BaseTableViewCell)?.tableCellSelectedDelegate = (self.parent as? BaseViewController)!

		// Set delete delegate
		(cell as? BaseTableViewCell)?.tableCellDeleteDelegate = (self.parent as? BaseViewController)!
		(cell as? BaseTableViewCell)?.tableCellDeleteDelegate = self

		cell.selectionStyle = .none

		return cell
	}

	// MARK:- Accordion

	/**
	For accordion we will store height for index path for open and close cells

	- parameter tableView: Table view
	- parameter indexPath: IndexPath

	- returns: Value for open or close height of cell
	*/
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
		if accordion != nil && accordion! {
            
			for accordionCell in accordionCellsData {

				if (accordionCell.index as NSIndexPath).row == (indexPath as NSIndexPath).row {

					return accordionCell.cellHeight
				}
			}

			return UITableViewAutomaticDimension
        } else if explicitRowheight != nil {
            return explicitRowheight!
        }
		else {

			return UITableViewAutomaticDimension
		}
	}

	/**
	For accordion, take height of new frame of selected cell

	- parameter tableView: Table view
	- parameter indexPath: Selected indexPath
	*/
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

		if accordion != nil && accordion! {

			tableView.beginUpdates()
			tableView.endUpdates()

			let selectedCell = tableView.cellForRow(at: indexPath)
			let accordionData = AccordionCellType.init(index: indexPath, cellHeight: (selectedCell?.frame.height)!)
			accordionCellsData.append(accordionData)
//			tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
		}

		/**
		*  For selectable tableview
		*/
		if selectableTableDelegate != nil{

			selectableTableDelegate?.selectableCellAction(indexPath)
		}
	}

	/**
	Collapse accordion cell

	- parameter tableView: Table view
	- parameter indexPath: Deselect cell
	*/
	override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {

		if accordion != nil && accordion! {

			for i in 0..<accordionCellsData.count {
				print(i)
				if (accordionCellsData[i].index as NSIndexPath).row == (indexPath as NSIndexPath).row {

					accordionCellsData.remove(at: i)
					break
				}
			}

			tableView.beginUpdates()
			tableView.endUpdates()
		}
	}

	// MARK:- Edit cell on swipe

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

		return editEnabled
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
	}

	override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

		var actions = [UITableViewRowAction]()

		for actionName in editActions {

			let actionRow = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: actionName, handler: { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in

				let sectionData = self.dataArray[indexPath.section]

				if let cellData = sectionData.sectionCellData![indexPath.row].cellData {
					self.swipeDelegate?.swipeAction(cellData, indexPath: indexPath, actionName: actionName)
				}
			})

			actionRow.backgroundColor = UIColor.clear

			actions.append(actionRow)
		}

		return actions
	}

	// MARK:- Remove cell from table view

	/// Remove cell from table view
	///
	/// - parameter data:      data to be removed
	/// - parameter indexPath: Remove at index path
	func deleteCell(_ data: AnyObject?, indexPath: IndexPath) {

		let section = self.dataArray[indexPath.section]
		section.sectionCellData?.remove(at: indexPath.row)

		DispatchQueue.main.async {

			self.tableView.beginUpdates()

			self.tableView.deleteRows(at: [indexPath], with: .automatic)

			self.tableView.endUpdates()
		}
	}

	// MARK:- Regsister cell

	/**
	If using xib for presenting cell, register it here for table view
	#Important: Use for nibName and cellReuseIdentifier same string!!
	*/
	func registerCells() {

		for section in self.dataArray {

			// Header cell register
			if let sectionCellName = section.sectionName {

				// Empty string is for creating section with zero height
				if sectionCellName != EMPTY_STRING {
					tableView.register(UINib(nibName: sectionCellName, bundle: nil), forHeaderFooterViewReuseIdentifier: sectionCellName)
				}
			}

			if let sectionData = section.sectionCellData {

				for cell in sectionData {

					// Register cell
					if let cellIdentifier = cell.cellName {
						registerCellForIdentifier(cellIdentifier)
					}
				}
			}
		}

		// If there is pagination, register loading cell
		if (self.loadingMore != nil) {
			registerCellForIdentifier("LoadingTableCell")
		}

	}

	/**
	Register cell for table view

	- parameter cellIdentifier: The reuse identifier
	*/
	func registerCellForIdentifier(_ cellIdentifier: String) {
		tableView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
	}
    
    //MARK:- Refresh on pull
    
    @IBAction func startReloadDataOnPull(_ sender: UIRefreshControl) {
        
        sender.endRefreshing()
        
        if let vc = parent as? BaseViewController{
            
            vc.loadingData()
        }
    }
    

}
