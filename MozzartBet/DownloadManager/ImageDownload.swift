//
//  ImageDownload.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

let categoryImageUrl = "http://static.mozzartsport.com/images/flags/26x17/"

class ImageDownload: NSObject {

    /// Set image for category
    ///
    /// - Parameters:
    ///   - categoryId: Caegory id
    ///   - iamgeView: ImageView for setting image
    class func getCategoryImage(categoryId: NSInteger, imageView: UIImageView){
        
        let image = UIImage(named: "default_category_icon")
        let url = URL(string: categoryImageUrl + "\(categoryId)")
        imageView.kf.setImage(with: url, placeholder: image)
    }
    
}
