//
//  EventView.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/28/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class EventView: UIView {

    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventPlayer: UILabel!
    @IBOutlet weak var eventTime: UILabel!

    
    func configView(event: AnyObject){
        
        // Card
        if let card = event as? Card{
        
            // Image
            if let color = card.color{
                eventImage.image = color == .yellow ? UIImage(named: "yellowCard") : UIImage(named: "redCard")
            }
            
            // Time
            if let time = card.time{
                
                eventTime.text = "\(time)"
            }
            
            // Player
            if let player = card.player{
                
                eventPlayer.text = player
            }
        }
        
        // Goal
        if let goal = event as? Goal{
            
            eventImage.image = UIImage(named: "goal")
            
            // Time
            if let time = goal.time{
                
                eventTime.text = "\(time)"
            }
            
            // Player
            if let player = goal.player{
                
                eventPlayer.text = player
            }
        }
    }
}


extension UIView{
    
    public class func fromNib(_ nibNameOrNil: String? = nil) -> Self {
        return fromNib(nibNameOrNil, type: self)
    }
    
    public class func fromNib<T : UIView>(_ nibNameOrNil: String? = nil, type: T.Type) -> T {
        let v: T? = fromNib(nibNameOrNil, type: T.self)
        return v!
    }
    
    public class func fromNib<T : UIView>(_ nibNameOrNil: String? = nil, type: T.Type) -> T? {
        var view: T?
        let name: String
        if let nibName = nibNameOrNil {
            name = nibName
        } else {
            // Most nibs are demangled by practice, if not, just declare string explicitly
            name = nibName
        }
        let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        for v in nibViews! {
            if let tog = v as? T {
                view = tog
            }
        }
        return view
    }
    
    public class var nibName: String {
        let name = "\(self)".components(separatedBy: ".").first ?? ""
        return name
    }
    public class var nib: UINib? {
        if let _ = Bundle.main.path(forResource: nibName, ofType: "nib") {
            return UINib(nibName: nibName, bundle: nil)
        } else {
            return nil
        }
    }
}
