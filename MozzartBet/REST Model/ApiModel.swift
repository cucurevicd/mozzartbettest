//
//  ApiModel.swift
//  Jukebox
//
//  Created by Dusan Cucurevic on 2/22/17.
//  Copyright © 2017 Home. All rights reserved.
//

import UIKit
import Gloss
import NVActivityIndicatorView
import Alamofire

//MARK:- Params

let baseUrl: String = "http://ws.mozzartsport.com/index.php/livescores.json?"
let baseUrlDetails: String = "http://ws.mozzartsport.com/matchcasts.json?"
let sportId: String = "sport_id"
let liveType: String = "type"
let fromTime : String = "from_time"
let untilTime : String = "until_time"
let matchId: String = "match_id"

class ApiModel: NSObject {
    
    typealias GamesHandler = (_ games: [Game]) -> Void
    
    // MARK:- Methods
    
    /// Get all games from any parameter scope
    ///
    /// - Parameter games: Array of games
    static func getGames(_ params: Parameters, _ games: @escaping GamesHandler){
        
        BaseAPI.execute(baseUrl: baseUrl, method: .get, service: EMPTY_STRING, params: params) { (finish, results) in
            
            print(results as Any)
            
            if finish{
                
                var allGames = [Game]()
                
                if let resultGames = results?.value(forKey: "livescores") as? [NSDictionary]{
                    
                    for game in resultGames{
                        
                        if let gameObject = Game(json: game as! JSON){
                         
                            allGames.append(gameObject)
                        }
                    }
                }
                
                games(allGames)
            }
        }
    }
    
    
    /// Get details and cast for game
    ///
    /// - Parameters:
    ///   - gameId: Id of game
    ///   - gameDetails: Return game with details
    static func getGameDetails(gameId: NSInteger, gameDetails: @escaping (_ game: Game?) -> Void ){
        
        BaseAPI.execute(baseUrl: baseUrlDetails, method: .get, service: EMPTY_STRING, params: [matchId:gameId]) { (finish, results) in
            
            print(results as Any)
            
            if finish{
                
                if let resultGames = results?.value(forKey: "matchcast") as? NSDictionary{
                    
                    if let gameObject = Game(json: resultGames as! JSON){
                        
                        gameDetails(gameObject)
                    }
                    else{
                        gameDetails(nil)
                    }
                }
                else{
                 
                    gameDetails(nil)
                }
            }
        }
    }
}
