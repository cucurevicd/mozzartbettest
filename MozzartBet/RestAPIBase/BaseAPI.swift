    //
    //  BaseAPI.swift
    //  DrivingTests
    //
    //  Created by Tomislav Jankovic on 10/19/15.
    //  Copyright © 2015 Intellex. All rights reserved.
    //
    
    import UIKit
    import Alamofire
    import NVActivityIndicatorView
    import Kingfisher
    
    //MARK: - Api constants
    
    //MARK:- Methods
    
    class BaseAPI: NSObject {
        
        
        // Execute API call
        class func execute(baseUrl:String, method: HTTPMethod, service: String, params: Parameters?, completion: @escaping (_ success: Bool,_ result: NSDictionary?) -> Void) {
            
            print(params as Any)
            
            // Create URL
            let url = String(format: "%@%@", baseUrl, service)
            
            Alamofire.request(url, method: method, parameters: params, encoding: URLEncoding.queryString).responseJSON { (response:DataResponse<Any>) in
                
                print(response.request as Any)
                
                // Stop loading
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                
                print(response)
    
                //Handle response
                
                if let statusCode = response.response?.statusCode{
                    
                    switch statusCode{
                    // Error code -999 is for canceled request
                    case -999:
                        completion(false, nil)
                        return
                        
                    // No internet connection
                    case  -1009:
                        
                        completion(false, nil)
                        return
                        
                    // Request timeout
                    case -1001:
                        completion(false, nil)
                        return
                        
                    case 200, 201:
                        
                        // If success
                        if response.result.isSuccess{
                            
                            if let responseDict = response.result.value as? NSDictionary{
                                completion(true, responseDict)
                            }
                            else{
                                completion(false, nil)
                                return
                            }
                        }
                            
                            // Handle network error
                        else{
                            
                            completion(false, nil)
                            return
                        }
                        break
                        
                    default:
                        if response.result.error != nil{
                            
                            print(response.result.error as Any)
                            Util.alertWithTitle("Error", message: (response.result.error?.localizedDescription)!)
                        }
                        else{
                            Util.alertWithTitle("Error", message: "Server error")
                        }
                        
                        completion(false, nil)
                        break
                    }
                }
                else{
                    
                    Util.alertWithTitle("Error", message: "Server error")
                    print("Unknow error")
                    completion(false, nil)
                }
            }
        }
        
    }
