//
//  GameDetailsViewController.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class GameDetailsViewController: BaseViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var teamFirstName: UILabel!
    @IBOutlet weak var teamSecondName: UILabel!
    
    @IBOutlet weak var currentResult: UILabel!
    @IBOutlet weak var teamFirstScore: UILabel!
    @IBOutlet weak var teamSecondScore: UILabel!
    
    // MARK:- Properties
    
    var gameId: NSInteger?{
        
        didSet{
            
            if gameId != nil{
                loadingData()
            }
        }
    }
    
    var game: Game?{
        
        didSet{
            
            configTable()
            configUI()
        }
    }
    
    
    //MARK:- View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // If it is testing details
        if let detailsTesting = UserDefaults.standard.value(forKey: "test_details") as? Bool{
            
            if detailsTesting{
                if let testingGameId = UserDefaults.standard.value(forKey: "match_id") as? NSInteger{
                    
                    self.gameId = testingGameId
                }
                else{
                    // Set default value
                    gameId = 12420200
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.navigationBar.isHidden = false
        DataController.sharedInstance.addObserver(self, forKeyPath: #keyPath(DataController.detailGame), options: .new, context: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        DataController.sharedInstance.removeObserver(self, forKeyPath: #keyPath(DataController.detailGame))
    }
    
    override func loadingData() {
        
        DataController.configGameDetailsController(gameId: gameId!)
        DataController.loadGameDetails()
    }
    
    //MARK:- Config UI
    
    func configTable(){
        
        var tableDatas = [TableCellData]()
        
        // Stats
        if let game = DataController.sharedInstance.detailGame{
        
            if let firstStats = game.firstTeamStats, let secondStats = game.secondTeamStats{
            
                let statsData = ["1" : firstStats, "2" : secondStats]
                let cellData = TableCellData(name: "StatsTableCell", data: statsData as AnyObject?)
                tableDatas.append(cellData)
            }
        }
        
        // See comments
        let cellDataComments = TableCellData(name: "SeeCommentsTableCell", data: nil)
        tableDatas.append(cellDataComments)
        
        // See lineup
        let cellDataLineup = TableCellData(name: "SeeLinupeTableCell", data: nil)
        tableDatas.append(cellDataLineup)
        
        self.data = tableDatas
    }
    
    func configUI(){
        
        if let name = game?.homeTeam?.name{
            
            self.teamFirstName.text = name
        }
        
        if let name = game?.guestTeam?.name{
            
            self.teamSecondName.text = name
        }
        
        if let scoreFirst = game?.currentTimeScore?.homeTeam{
            
            self.teamFirstScore.text = "\(scoreFirst)"
        }
        
        if let scoreSecond = game?.currentTimeScore?.guestTeam{
            
            self.teamSecondScore.text = "\(scoreSecond)"
        }
        
        if let time = game?.matchTime{
        
            self.currentResult.text = time
        }
    }
    
    // MARK:- Observer
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(DataController.detailGame) {
            
            self.game = DataController.sharedInstance.detailGame
        }
    }
}
