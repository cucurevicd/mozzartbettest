//
//  BaseViewController.swift
//  Quiz4U
//
//  Created by Intellex on 8/15/16.
//  Copyright © 2016 Intellex. All rights reserved.
//

import UIKit
import Foundation

let headerViewIdentifier = "ViewControllerHeaderView"

class BaseViewController: UIViewController, LoadingDataDelegate, TableCellSelectedProtocol, TableCellDeleteDelegate, CollectionCellSelectedProtocol {

	var firstRespondTextField: UITextField?
	
	let topViewController: UIViewController! = UIApplication.topViewController()

	// Loading flag
	var loadMore : Bool?
    
    var tableVC: CustomTableViewController?
    var data: [TableCellData]?{
        
        didSet{
            
            let section = TableSectionData(sectionName: nil, data: nil, cellData: data)
            tableVC?.dataArray = [section]
            
            DispatchQueue.main.async {
                self.tableVC?.tableView.reloadData()
                
                if (self.data?.count)! > 0{
                    self.tableVC?.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.none, animated: true)
                }
                else{
                    // TODO: Add view for no Games
                }
            }
        }
    }

	
	// MARK:- View cycle
    
	override func viewDidLoad() {
		super.viewDidLoad()
       
	}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DataController.sharedInstance.addObserver(self, forKeyPath: #keyPath(DataController.games), options: .new, context: nil)
        
       self.navigationController?.navigationBar.isHidden = true
        
        // Load data
        loadingData()
    }

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        DataController.sharedInstance.removeObserver(self, forKeyPath: #keyPath(DataController.games))
    }
	
    
    // MARK:- Observer
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(DataController.games) {
            
            var tableDatas = [TableCellData]()
            
            for game in DataController.sharedInstance.games!{
                
                let cellData = TableCellData(name: "GameTableCell", data: game)
                tableDatas.append(cellData)
            }
            
            self.data = tableDatas
        }
    }
	
	// MARK:- Custom navigation bar view delegate
	/**
	CustomNavigationBarDelegate implemented, disimiss or pop VC. Delete EventListener from the ListenerPool
	
	- parameter viewController: ViewController to to be dismissed/popped for which event listeners will be deleted
	*/
	func goBack(_ viewController: UIViewController) {
		
		let isModal = self.isModal()
		
		DispatchQueue.main.async {
			if isModal {
				
				self.dismiss(animated: true, completion: nil)
				
			} else {
				_ = self.navigationController?.popViewController(animated: true)
			}
		}
		
	}
	
	/**
	Open or close side menu VC
	*/
	func sideMenuAction() {
		
		let storyBoard = UIStoryboard.init(name: "SideMenu", bundle: Bundle.main)
		let sideMenuVC = storyBoard.instantiateInitialViewController()
		self.presentNext(sideMenuVC!)
	}
	
	/**
	Back to root view controlelr
	*/
	func goHome() {

       _ = self.navigationController?.popToRootViewController(animated: true)
	}
	
    
    /// Go to main game screen
    func goMainScreen(){
        
        DispatchQueue.main.async {
            
            let homeVC = Util.VC("Main", vcName: "HomeViewController")
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }

	
	// MARK: - View controller transition next
	
	/**
	Push view controller to next VC

	- parameter viewController: <#viewController description#>
	*/
	func goNext(_ viewController: UIViewController) {
		
		self.navigationController?.pushViewController(viewController, animated: true)
	}
	
	/**
	Present next VC

	- parameter viewController: <#viewController description#>
	*/
	func presentNext(_ viewController: UIViewController) {
		
		self.present(viewController, animated: true, completion: nil)
	}
	
	// MARK: - Loading data
	
	/**
	Delegate method for loading data for pagination cell
	*/
	func loadingData() {
		
        DataController.loadGames()
	}
    
    /// Use this method to refresh data on page. Every View controller will define its
    func refreshPage(){
        
        self.view.layoutSubviews()
    }
	
    //MARK:- Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination.isKind(of: CustomTableViewController.self){
            
            self.tableVC = segue.destination as? CustomTableViewController
        }
    }
    
	// MARK:- Table view cell select protocol
	
	/**
	Table cell is selected method

	- parameter data: Table cell data as TableCellData
	*/
	func tableCellSelected(_ data: AnyObject?) {
		
	}
	
	/**
	Table cell is selected for specific path

	- parameter data:      Table cell data
	- parameter indexPath: Selected index path
	*/
	func tableCellSelectedAtIndexPath(_ data: AnyObject?, indexPath: IndexPath) {
		
	}
	
	/**
	Abstract function for delete cell

	- parameter data:      Cell data
	- parameter indexPath: Cell index path
	*/
	func deleteCell(_ data: AnyObject?, indexPath: IndexPath) {
		
	}

	// MARK:- Collection view cell delegate


	/// Abstract func for selected collection cell
	///
	/// - parameter data:      Collection cell data
	/// - parameter indexPath: Collection cell index path
	func collectionCellSelected(_ data: AnyObject?, indexPath: IndexPath) {

	}
	
	// MARK:- Memory management
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	}

