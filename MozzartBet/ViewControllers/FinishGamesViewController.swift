//
//  FinishGamesViewController.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/28/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class FinishGamesViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        
        // Do any additional setup after loading the view.
        DataController.configDataController(type: .finish)
        
        super.viewWillAppear(animated)
    }
}
