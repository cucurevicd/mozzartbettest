//
//  SpecificDetailsViewController.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit

class SpecificDetailsViewController: BaseViewController {
    
    
    enum SpecificDetails {
        case comments
        case lineup1
        case lineup2
    }
    
    var gameId: NSInteger?
    var specificDetails: SpecificDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataController.configGameDetailsController(gameId: DataController.sharedInstance.observerdGameId!)
        configTable()
    }

    override func viewWillAppear(_ animated: Bool) {
    
        self.navigationController?.navigationBar.isHidden = false
        DataController.sharedInstance.addObserver(self, forKeyPath: #keyPath(DataController.detailGame), options: .new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        DataController.sharedInstance.removeObserver(self, forKeyPath: #keyPath(DataController.detailGame))
    }
    
    // MARK:- Observer
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(DataController.detailGame) {
            
            configTable()
        }
    }
    
    //MARK:- Config UI
    
    func configTable(){
        
        var tableDatas = [TableCellData]()
        
        let array : [AnyObject]?
        let cellName: String?
        
        switch specificDetails! {
        case .comments:
            array = DataController.sharedInstance.detailGame?.comments
            cellName = "CommentTableCell"
            break
            
        case .lineup1:
            array = DataController.sharedInstance.detailGame?.homeTeam?.lineup?.onfield
            cellName = "PlayerTableCell"
            break
            
        case .lineup2:
            array = DataController.sharedInstance.detailGame?.guestTeam?.lineup?.onfield
            cellName = "PlayerTableCell"
            break
        }
        
        if array != nil{
            for data in array!{
                
                let cellData = TableCellData(name: cellName!, data: data)
                tableDatas.append(cellData)
            }
        }
        else{
            Util.alertWithTitle("Error", message: "Data has been set yet")
        }
        
        self.data = tableDatas
            
    }
}
