//
//  Goal.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/27/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Goal: NSObject, Decodable {

    
    // MARK:- Properties
    
    var player: String?
    var score: String?
    var team: String?
    var time: NSInteger?
    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        self.player = "player" <~~ json
        self.score = "score" <~~ json
        self.team = "team" <~~ json
        self.time = "time" <~~ json
    }
}
