//
//  Team.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/27/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Team: NSObject, Decodable {

    //MARK:- Properties
    
    var name: String?
    var teamId: NSInteger?
    
    var colors: TeamColors?
    var formation: String?
    
    var lineup: Lineup?
    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        self.name = "name" <~~ json
        self.teamId = "team_id" <~~ json
        
        // For match details
        self.colors = "colors" <~~ json
        self.formation = "formation" <~~ json
        self.lineup = "lineup" <~~ json
    }
}
