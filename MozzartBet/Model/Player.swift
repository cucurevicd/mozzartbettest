//
//  Player.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Player: NSObject, Decodable {

    
    //MARK:- Properties
    var id: NSInteger?
    var name: String?
    var number: NSInteger?
    var position: String?
    
    var orderNo: NSInteger?
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        self.id = "id" <~~ json
        self.name = "name" <~~ json
        self.number = "number" <~~ json
        self.position = "position" <~~ json
    }
}
