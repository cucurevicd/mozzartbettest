//
//  Score.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/27/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Score: NSObject, Decodable {

    
    //MARK:- Properties
    var guestTeam: NSInteger?
    var homeTeam: NSInteger?
    
    required init?(json: JSON) {
     
        self.guestTeam = "guest_team" <~~ json
        self.homeTeam = "home_team" <~~ json
    }
}
