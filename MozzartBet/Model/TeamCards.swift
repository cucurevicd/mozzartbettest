//
//  TeamCards.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/27/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class TeamCards: NSObject, Decodable {

    //MARK:- Properties
    var yellowCards: [Card]?
    var redCards: [Card]?
    
    
    required init?(json: JSON) {
        
        self.yellowCards = "Yellow" <~~ json
        self.redCards = "Red" <~~ json
        
        if yellowCards != nil{
            
            for card in yellowCards!{
                card.color = .yellow
            }
        }
        
        if redCards != nil{
            
            for card in redCards!{
                card.color = .red
            }
        }
    }
}
