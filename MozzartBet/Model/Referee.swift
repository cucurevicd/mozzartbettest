//
//  Referee.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Referee: NSObject, Decodable {

    
    //MARK:- Properties
    
    var countryId: NSInteger?
    var countryName: String?
    var name: String?
    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        self.countryId = "country_id" <~~ json
        self.countryName = "country_name" <~~ json
        self.name = "name" <~~ json
    }
}
