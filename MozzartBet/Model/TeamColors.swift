//
//  TeamColors.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class TeamColors: NSObject, Decodable {

    //MARK:- Properties
    var keeperColor: Color?
    var playerColor: Color?
    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        self.keeperColor = "keeper" <~~ json
        self.playerColor = "player" <~~ json
    }
}
