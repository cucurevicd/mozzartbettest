//
//  Cards.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/27/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Card: NSObject, Decodable {

    enum CardType{
        case yellow
        case red
    }
    
    // MARK:- Properties
    var color: CardType?
    var player: String?
    var time: NSInteger?

    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        self.player = "player" <~~ json
        self.time = "time" <~~ json
    }
}
