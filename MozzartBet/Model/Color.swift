//
//  Color.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Color: NSObject, Decodable {

    //MARK:- Properties
    
    var main: String?
    var number: String?
    var shirtType: String?
    var sleeve: String?
    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        self.main = "main" <~~ json
        self.number = "number" <~~ json
        self.shirtType = "shirtType" <~~ json
        self.sleeve = "sleeve" <~~ json
    }
    
}
