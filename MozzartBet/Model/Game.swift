//
//  Game.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/27/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Game: NSObject, Decodable  {

    // MARK:- Properties
    
    var homeTeamCards: TeamCards?
    var guestTeamcards: TeamCards?
    var categoryId: NSInteger?
    var categoryName : String?
    var goalsTime: [Goal]?
    var goalsGuest: [Goal]?
    var goalsHome:[Goal]?
    var homeTeam: Team?
    var guestTeam: Team?
    var matchCurrentTime: AnyObject?
    var matchId : NSInteger?
    var matchTime: String?
    var periodStarted: NSInteger?
    var currentTimeScore: Score?
    var halfTimeScore: Score?
    var finalTimeScore : Score?
    var started: NSInteger?
    var status: String?
    var statusCode: NSInteger?
    var tournamentId: NSInteger?
    var tournamentName : String?
    var winner: NSInteger?
    
    // Details
    var comments: [Comment]?
    var referee : Referee?
    
    var change: Bool?
    
    var firstTeamStats: Stats?
    var secondTeamStats: Stats?
    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        if let data = json.valueForKeyPath(keyPath: "cards") as? JSON{
            
            if let homeCards = data.valueForKeyPath(keyPath: "home_team") as? JSON{
                
                self.homeTeamCards = TeamCards(json: homeCards)
            }
            
            if let guestCards = data.valueForKeyPath(keyPath: "guest_team") as? JSON{
                
                self.guestTeamcards = TeamCards(json: guestCards)
            }
        }
        
        
        self.categoryId = "category_id" <~~ json
        self.categoryName = "category_name" <~~ json
        
        if let data = json.valueForKeyPath(keyPath: "goals") as? JSON{

                self.goalsTime = "by_time" <~~ data
                self.goalsHome = "home_team" <~~ data
                self.goalsGuest = "guest_team" <~~ data
        }
        
        self.homeTeam = "home_team" <~~ json
        self.guestTeam = "guest_team" <~~ json
        self.matchCurrentTime = "match_current_time" <~~ json
        self.matchId = "match_id" <~~ json
        self.matchTime = "match_time" <~~ json
        self.periodStarted = "period_started" <~~ json
        
        if let data = json.valueForKeyPath(keyPath: "score") as? JSON{
         
            if let _ = data.valueForKeyPath(keyPath: "current") as? JSON{
                
                self.currentTimeScore = "current" <~~ data
            }
            
            if let _ = data.valueForKeyPath(keyPath: "half_time") as? JSON{
                
                self.halfTimeScore = "half_time" <~~ data
            }

            if let _ = data.valueForKeyPath(keyPath: "normal_time") as? JSON{
                
                self.finalTimeScore = "normal_time" <~~ data
            }
        }
        
        self.started = "started" <~~ json
        self.status = "status" <~~ json
        self.statusCode = "status_code" <~~ json
        self.tournamentId = "tournament_id" <~~ json
        self.tournamentName = "tournament_name" <~~ json
        self.winner = "winner" <~~ json
        
        
        // Appendix details
        self.comments = [Comment]()
        
        // Comments
        if let commentsDict = json.valueForKeyPath(keyPath: "comments") as? NSDictionary{
            
            for key in commentsDict.allKeys{
                
                if let comment = Comment(json: commentsDict[key] as! JSON){
                    
                    comment.orderNo = NSInteger(key as! String)
                    self.comments?.append(comment)
                }
            }
        }
        
        // Sort comments
        self.comments?.sort{$0.orderNo! < $1.orderNo!}
        
        
        
        // Referee
        self.referee = "referee" <~~ json
        
        // Stats
        if let statsDict = json.valueForKeyPath(keyPath: "stats") as? NSDictionary{
            
            if statsDict["1"] != nil{
                
                self.firstTeamStats = "1" <~~ (statsDict as! JSON)
            }
            
            if statsDict["2"] != nil{
                
                self.secondTeamStats = "2" <~~ (statsDict as! JSON)
            }
        }
    }
    
}
