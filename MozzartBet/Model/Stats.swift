//
//  Stats.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Stats: NSObject, Decodable {

    //MARK:- Properties
    var assists: NSInteger?
    var blockedShots: NSInteger?
    var corners: NSInteger?
    var fouls: NSInteger?
    var offsides: NSInteger?
    var redCards: NSInteger?
    var shotsOffTarget: NSInteger?
    var shotsOnTarget: NSInteger?
    var yellowCards: NSInteger?
    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
     
        self.assists = "total" <~~ ("assists" <~~ json)!
        self.blockedShots = "total" <~~ ("blocked_shots" <~~ json)!
        self.corners = "total" <~~ ("corners" <~~ json)!
        self.fouls = "total" <~~ ("fouls" <~~ json)!
        self.offsides = "total" <~~ ("offsides" <~~ json)!
        self.redCards = "total" <~~ ("offsides" <~~ json)!
        self.shotsOffTarget = "total" <~~ ("shots_off_target" <~~ json)!
        self.shotsOnTarget = "total" <~~ ("shots_on_target" <~~ json)!
        self.yellowCards = "total" <~~ ("yellow_cards" <~~ json)!
    }
}
