//
//  Lineup.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Lineup: NSObject, Decodable {

    //MARK:- Properties
    var onfield: [Player]?
    var substitution: [Player]?
    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        // Onfiled players
        onfield = [Player]()
        
        if let onfieldPlayers = json.valueForKeyPath(keyPath: "on_field") as? NSDictionary{
            
            for key in onfieldPlayers.allKeys{
                
                if let player = Player(json: onfieldPlayers[key] as! JSON){
                    
                    player.orderNo = NSInteger(key as! String)
                    onfield?.append(player)
                }
            }
            
            //Sort
            onfield?.sort{$0.orderNo! < $1.orderNo!}
        }
        
        //Substitutions
        substitution = [Player]()
        
        if let substiotutionPlayers = json.valueForKeyPath(keyPath: "substitutions") as? NSDictionary{
            
            for key in substiotutionPlayers.allKeys{
                
                if let player = Player(json: substiotutionPlayers[key] as! JSON){
                    
                    player.orderNo = NSInteger(key as! String)
                    substitution?.append(player)
                }
            }
            
            //Sort
            substitution?.sort{$0.orderNo! < $1.orderNo!}
        }
        
    }
}
