//
//  Comment.swift
//  MozzartBet
//
//  Created by Dusan Cucurevic on 3/29/17.
//  Copyright © 2017 DusanCucurevic. All rights reserved.
//

import UIKit
import Gloss

class Comment: NSObject, Decodable {

    //MARK:- Properties
    
    var eventId: NSInteger?
    var eventTypeId: String?
    var funfact: NSInteger?
    var text: String?
    var time: AnyObject?
    var type: NSInteger?
    
    var orderNo: NSInteger?
    
    /**
     Deserialize object
     
     - returns: The Account balance record
     */
    required init?(json: JSON) {
        
        self.eventId = "event_id" <~~ json
        self.eventTypeId = "event_type_id" <~~ json
        self.funfact = "funfact" <~~ json
        self.text = "text" <~~ json
        self.time = "time" <~~ json
        self.type = "type" <~~ json
    }
}
