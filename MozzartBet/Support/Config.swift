//
//  Config.swift
//  EKapija
//
//  Created by Intellex on 12/16/15.
//  Copyright © 2015 Intellex. All rights reserved.
//

import Foundation
import UIKit


//CONST
let MAIN_SCREEN = UIScreen.main
let SCREEN_WIDTH = UIScreen.main.bounds.width
let SCREEN_HEIGHT = UIScreen.main.bounds.height
let DELEGATE = UIApplication.shared.delegate as! AppDelegate
let ERROR = NSLocalizedString("Error", comment: "error")
let NOT_IMPLEMENTED = NSLocalizedString("Not implemented", comment: "Not implemented")
let NO_INTERNET_CONNECTION = NSLocalizedString("No internet connection", comment: "No internet connection")
let SUCCESS = NSLocalizedString("Success", comment: "success")
let OK = NSLocalizedString("OK", comment: "ok")
let YES = NSLocalizedString("YES", comment: "yes")
let NO = NSLocalizedString("NO", comment: "no")
let CANCEL = NSLocalizedString("Cancel", comment: "cancel")
let SERVER_ERROR = NSLocalizedString("Server error", comment: "server error")
let ALERT = NSLocalizedString("Alert", comment: "alert message title")
let EMPTY_STRING = ""
let APP_NAME = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as! String

//NSUser defaults const
let DEVICE_TOKEN = "device_token"
let AUTH_TOKEN = "auth_token"

/**
*  Date formate
*/
let DATE_FORMAT = "yyyy-MM-dd HH:mm:ssxxx"
