//
//  ErrorHandler.swift
//  Quiz4U
//
//  Created by Aleksandar Ivanovic on 1/27/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class ErrorMessageHandler: Error {
    
   static var sharedInstance = ErrorMessageHandler()
    
   enum ErrorsEnum: String {
        
        /// Type: Server; Service: All; COMMON
        case ERR_FATAL_ERROR = "ERR_FATAL_ERROR"
        
        /// Type: Server; Service: All; COMMON
        case ERR_SESSION_STORE_INIT = "ERR_SESSION_STORE_INIT"
        
        /// Type: Server; Service: Java; COMMON
        case ERR_SESSION_STORE_DESTROY = "ERR_SESSION_STORE_DESTROY"
        
        /// Type: Server; Service: All; COMMON
        case ERR_CONNECTION_ERROR = "ERR_CONNECTION_ERROR"
        
        /// Type: Server; Service: Java; COMMON
        case ERR_SHUTTING_DOWN = "ERR_SHUTTING_DOWN"
        
        /// Type: Validation; Service: All; COMMON
        case ERR_VALIDATION_FAILED = "ERR_VALIDATION_FAILED"
        
        /// Type: Client; Service: All; COMMON
        case ERR_ENTRY_ALREADY_EXISTS = "ERR_ENTRY_ALREADY_EXISTS"
        
        /// Type: Client; Service: All; COMMON
        case ERR_ENTRY_NOT_FOUND = "ERR_ENTRY_NOT_FOUND"
        
        /// Type: Auth; Service: NodeJS, QuestionAPI; COMMON
        case ERR_TOKEN_NOT_VALID = "ERR_TOKEN_NOT_VALID"
        
        /// Type: Auth; Service: NodeJS, QuestionAPI; COMMON
        case ERR_CODE_NOT_VALID = "ERR_CODE_NOT_VALID"
        
        /// Type: Client; Service: NodeJS; COMMON
        case ERR_CONSTRAINTS = "ERR_CONSTRAINTS"
        
        /// Type: Server; Service: NodeJS, QuestionAPI, AuthApi; COMMON
        case ERR_FUNCTION_NOT_IMPLEMENTED = "ERR_FUNCTION_NOT_IMPLEMENTED"
        
        /// Type: Client; Service: NodeJS; COMMON
        case ERR_APPLICATION_IS_DEPLOYED = "ERR_APPLICATION_IS_DEPLOYED"
        
        /// Type: Server; Service: NodeJS; COMMON
        case ERR_DATABASE_NO_MODEL_DEFINED = "ERR_DATABASE_NO_MODEL_DEFINED"
        
        /// Type: Server; Service: NodeJS; COMMON
        case ERR_DATABASE_NO_ENTITY_DATA = "ERR_DATABASE_NO_ENTITY_DATA"
        
        /// Type: Client; Service: NodeJS, AuthApi; COMMON
        case ERR_AUTH_NO_REGISTERED = "ERR_AUTH_NO_REGISTERED"
        
        /// Type: Client; Service: NodeJS, AuthApi; COMMON
        case ERR_AUTH_ALREADY_REGISTERED = "ERR_AUTH_ALREADY_REGISTERED"
        
        /// Type: Client; Service: NodeJS, AuthApi; COMMON
        case ERR_AUTH_PROVIDER_TOKEN_NOT_VALID = "ERR_AUTH_PROVIDER_TOKEN_NOT_VALID"
        
        /// Type: Client; Service: NodeJS, AuthApi; COMMON
        case ERR_AUTH_PROVIDER_NO_EMAIL_RETURNED = "ERR_AUTH_PROVIDER_NO_EMAIL_RETURNED"
        
        /// Type: Server; Service: NodeJS, AuthApi; COMMON
        case ERR_AUTH_PROVIDER_SERVICE_FATAL_ERROR = "ERR_AUTH_PROVIDER_SERVICE_FATAL_ERROR"
        
        /// Type: Server; Service: NodeJS; COMMON
        case ERR_AUTH_VERIFICATION_CODE_ERROR = "ERR_AUTH_VERIFICATION_CODE_ERROR"
        
        /// Type: Server; Service: NodeJS; COMMON
        case ERR_NO_INSTANCE_AVAILABLE = "ERR_NO_INSTANCE_AVAILABLE"
        
        /// Type: Client; Service: NodeJS, AuthApi; COMMON
        case ERR_AUTH_SECRET_WRONG = "ERR_AUTH_SECRET_WRONG"
        
        /// Type: Server; Service: NodeJS; COMMON
        case ERR_DATABASE_NO_RECORD_ID = "ERR_DATABASE_NO_RECORD_ID"
        
        /// Type: Server; Service: NodeJS; COMMON
        case ERR_DATABASE_NO_ENGINE = "ERR_DATABASE_NO_ENGINE"
        
        /// Type: Server; Service: NodeJS; COMMON
        case ERR_DATABASE_NO_FUNCTION = "ERR_DATABASE_NO_FUNCTION"
        
        /// Type: Server; Service: NodeJS; COMMON
        case ERR_DATABASE_NO_FUNCTION_CALLBACK = "ERR_DATABASE_NO_FUNCTION_CALLBACK"
        
        /// Type: Client; Service: NodeJS; COMMON
        case ERR_SERVICE_NOT_FOUND = "ERR_SERVICE_NOT_FOUND"
        
        /// Type: Client; Service: NodeJS, QuestionAPI; COMMON
        case ERR_ENTITY_CONTAIN_BAD_WORDS = "ERR_ENTITY_CONTAIN_BAD_WORDS"
        
        /// Type: Server; Service: NodeJS; COMMON
        case SESSION_STORE_INIT = "SESSION_STORE_INIT"
        
        /// Type: Server; Service: NodeJS; COMMON
        case SESSION_STORE_DESTROY = "SESSION_STORE_DESTROY"
        
        /// Type: Server; Service: NodeJS; COMMON
        case NO_SERVICE_REGISTRY_SPECIFIED = "NO_SERVICE_REGISTRY_SPECIFIED"
        
        /// Type: Server; Service: NodeJS; COMMON
        case SERVICE_CONNECTION_INFORMATION_NOT_FOUND = "SERVICE_CONNECTION_INFORMATION_NOT_FOUND"
        
        /// Type: Client; Service: NodeJS; COMMON
        case VOUCHER_ALREADY_USED = "VOUCHER_ALREADY_USED"
        
        /// Type: Client; Service: NodeJS; COMMON
        case COMPONENT_ALREADY_USED = "COMPONENT_ALREADY_USED"
        
        /// Type: Server; Service: UserMessageClient; Specific
        case ERR_CALLBACK_NOT_PROVIDED = "ERR_CALLBACK_NOT_PROVIDED"
        
        /// Type: Server; Service: UserMessageClient; Specific
        case ERR_WRONG_REGISTRY_SERVICE_URIS = "ERR_WRONG_REGISTRY_SERVICE_URIS"
        
        /// Type: Client; Service: QuestionAPI; Specific
        case ERR_QUESTION_NO_RESOLUTION = "ERR_QUESTION_NO_RESOLUTION"
        
        /// Type: Client; Service: QuestionAPI; Specific
        case ERR_QUESTION_HAS_NO_POOL = "ERR_QUESTION_HAS_NO_POOL"
        
        /// Type: Client; Service: QuestionAPI; Specific
        case ERR_QUESTION_HAS_NO_TEMPLATE = "ERR_QUESTION_HAS_NO_TEMPLATE"
        
        /// Type: Client; Service: QuestionAPI; Specific
        case ERR_NO_PUBLISHED_TRANSLATION_FOUND = "ERR_NO_PUBLISHED_TRANSLATION_FOUND"
        
        /// Type: Client; Service: QuestionAPI; Specific
        case ERR_QUESTION_TRANSLATION_CONTENT_HAS_NO_STEPS_MAPPINGS = "ERR_QUESTION_TRANSLATION_CONTENT_HAS_NO_STEPS_MAPPINGS"
        
        /// Type: Server; Service: QuestionAPI, MediaApi; Specific
        case ERR_FOREIGN_KEY_CONSTRAINT_VIOLATION = "ERR_FOREIGN_KEY_CONSTRAINT_VIOLATION"
        
        /// Type: Server; Service: QuestionAPI, MediaApi; Specific
        case ERR_UNIQUE_KEY_CONSTRAINT_VIOLATION = "ERR_UNIQUE_KEY_CONSTRAINT_VIOLATION"
        
        /// Type: Client; Service: QuestionAPI; Specific
        case MEDIA_NOT_EXIST = "MEDIA_NOT_EXIST"
        
        /// Type: Client; Service: QuestionAPI; Specific
        case MEDIA_COULD_NOT_BE_UPLOADED = "MEDIA_COULD_NOT_BE_UPLOADED"
        
        /// Type: Client; Service: PoolApi; Specific
        case ERR_POOL_HIERARCHY_INCONSISTENT = "ERR_POOL_HIERARCHY_INCONSISTENT"
        
        /// Type: Client; Service: WorkorderApi; Specific
        case ERR_WORKORDER_HAS_NO_POOL = "ERR_WORKORDER_HAS_NO_POOL"
        
        /// Type: Client; Service: ApplicationApi; Specific
        case ERR_APPLICATION_IS_ACTIVATED = "ERR_APPLICATION_IS_ACTIVATED"
        
        /// Type: Client; Service: ApplicationApi; Specific
        case ERR_APPLICATION_IS_DEACTIVATED = "ERR_APPLICATION_IS_DEACTIVATED"
        
        /// Type: Client; Service: ApplicationApi; Specific
        case ERR_APPLICATION_IS_ARCHIVED = "ERR_APPLICATION_IS_ARCHIVED"
        
        /// Type: Client; Service: ApplicationApi; Specific
        case ERR_APPLICATION_NAME_NOT_UNIQUE_PER_TENANT = "ERR_APPLICATION_NAME_NOT_UNIQUE_PER_TENANT"
        
        /// Type: Client; Service: ApplicationApi; Specific
        case ERR_APPLICATION_HAS_NO_PAYMENT_TYPE = "ERR_APPLICATION_HAS_NO_PAYMENT_TYPE"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_IS_DEPLOYED = "ERR_GAME_IS_DEPLOYED"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_IS_ACTIVATED = "ERR_GAME_IS_ACTIVATED"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_IS_DEACTIVATED = "ERR_GAME_IS_DEACTIVATED"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_IS_ARCHIVED = "ERR_GAME_IS_ARCHIVED"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_HAS_NO_TEMPLATE = "ERR_GAME_HAS_NO_TEMPLATE"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_HAS_NO_APPLICATION = "ERR_GAME_HAS_NO_APPLICATION"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_CONFIGURATION_IS_INVALID = "ERR_GAME_CONFIGURATION_IS_INVALID"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_GAME_FOUND = "ERR_GAME_PUBLISHING_NO_GAME_FOUND"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_APPLICATION_FOUND = "ERR_GAME_PUBLISHING_NO_APPLICATION_FOUND"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_POOL_FOUND = "ERR_GAME_PUBLISHING_NO_POOL_FOUND"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_REGIONAL_SETTING_FOUND = "ERR_GAME_PUBLISHING_NO_REGIONAL_SETTING_FOUND"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_LANGUAGE_FOUND = "ERR_GAME_PUBLISHING_NO_LANGUAGE_FOUND"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_QUESTION_TEMPLATE_FOUND = "ERR_GAME_PUBLISHING_NO_QUESTION_TEMPLATE_FOUND"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_ADVERTISEMENT_FOUND = "ERR_GAME_PUBLISHING_NO_ADVERTISEMENT_FOUND"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_VOUCHER_FOUND = "ERR_GAME_PUBLISHING_NO_VOUCHER_FOUND"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_WINNING_COMPONENT_FOUND = "ERR_GAME_PUBLISHING_NO_WINNING_COMPONENT_FOUND"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_NO_ENTRY_FEE_AMOUNT = "ERR_GAME_PUBLISHING_NO_ENTRY_FEE_AMOUNT"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_INVALID_AMOUNT_OF_QUESTIONS = "ERR_GAME_PUBLISHING_INVALID_AMOUNT_OF_QUESTIONS"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PUBLISHING_INVALID_COMPLEXITY_STRUCTURE = "ERR_GAME_PUBLISHING_INVALID_COMPLEXITY_STRUCTURE"
        
        /// Type: Client; Service: BillingApi; Specific
        case ERR_NO_BILL_SELECTED = "ERR_NO_BILL_SELECTED"
        
        /// Type: Client; Service: BillingApi; Specific
        case ERR_BILL_NOT_APPROVED = "ERR_BILL_NOT_APPROVED"
        
        /// Type: Client; Service: AdvertisementApi; Specific
        case ERR_ADVERTISEMENT_IS_DEPLOYED = "ERR_ADVERTISEMENT_IS_DEPLOYED"
        
        /// Type: Client; Service: AdvertisementApi; Specific
        case ERR_ADVERTISEMENT_IS_ACTIVATED = "ERR_ADVERTISEMENT_IS_ACTIVATED"
        
        /// Type: Client; Service: AdvertisementApi; Specific
        case ERR_ADVERTISEMENT_IS_DEACTIVATED = "ERR_ADVERTISEMENT_IS_DEACTIVATED"
        
        /// Type: Client; Service: AdvertisementApi; Specific
        case ERR_ADVERTISEMENT_IS_ARCHIVED = "ERR_ADVERTISEMENT_IS_ARCHIVED"
        
        /// Type: Client; Service: AdvertisementApi; Specific
        case ERR_ADVERTISEMENT_PROVIDER_IS_ACTIVATED = "ERR_ADVERTISEMENT_PROVIDER_IS_ACTIVATED"
        
        /// Type: Client; Service: AdvertisementApi; Specific
        case ERR_ADVERTISEMENT_PROVIDER_IS_DEACTIVATED = "ERR_ADVERTISEMENT_PROVIDER_IS_DEACTIVATED"
        
        /// Type: Client; Service: AdvertisementApi; Specific
        case ERR_ADVERTISEMENT_PROVIDER_IS_ARCHIVED = "ERR_ADVERTISEMENT_PROVIDER_IS_ARCHIVED"
        
        /// Type: Client; Service: AdvertisementApi; Specific
        case ERR_CONTENT_IS_PUBLISHED = "ERR_CONTENT_IS_PUBLISHED"
        
        /// Type: Client; Service: VoucherApi; Specific
        case ERR_VOUCHER_IS_DEPLOYED = "ERR_VOUCHER_IS_DEPLOYED"
        
        /// Type: Client; Service: VoucherApi; Specific
        case ERR_VOUCHER_IS_ACTIVATED = "ERR_VOUCHER_IS_ACTIVATED"
        
        /// Type: Client; Service: VoucherApi; Specific
        case ERR_VOUCHER_IS_DEACTIVATED = "ERR_VOUCHER_IS_DEACTIVATED"
        
        /// Type: Client; Service: VoucherApi; Specific
        case ERR_VOUCHER_IS_ARCHIVED = "ERR_VOUCHER_IS_ARCHIVED"
        
        /// Type: Client; Service: VoucherApi; Specific
        case ERR_VOUCHER_PROVIDER_IS_ACTIVATED = "ERR_VOUCHER_PROVIDER_IS_ACTIVATED"
        
        /// Type: Client; Service: VoucherApi; Specific
        case ERR_VOUCHER_PROVIDER_IS_DEACTIVATED = "ERR_VOUCHER_PROVIDER_IS_DEACTIVATED"
        
        /// Type: Client; Service: VoucherApi; Specific
        case ERR_VOUCHER_PROVIDER_IS_ARCHIVED = "ERR_VOUCHER_PROVIDER_IS_ARCHIVED"
        
        /// Type: Client; Service: VoucherApi; Specific
        case ERR_VOUCHER_CODES_ARE_NOT_AVAILABLE = "ERR_VOUCHER_CODES_ARE_NOT_AVAILABLE"
        
        /// Type: Client; Service: ProfileApi; Specific
        case ERR_ALREADY_VALIDATED = "ERR_ALREADY_VALIDATED"
        
        /// Type: Validation; Service: MediaApi; Specific
        case ERR_MEDIA_API_VALIDATION_FAILED = "ERR_MEDIA_API_VALIDATION_FAILED"
        
        /// Type: Client; Service: MediaApi; Specific
        case ERR_MEDIA_API_CDN_UPLOAD = "ERR_MEDIA_API_CDN_UPLOAD"
        
        /// Type: Client; Service: MediaApi; Specific
        case ERR_MEDIA_API_CDN_READ = "ERR_MEDIA_API_CDN_READ"
        
        /// Type: Client; Service: MediaApi; Specific
        case ERR_MEDIA_API_CDN_DELETE = "ERR_MEDIA_API_CDN_DELETE"
        
        /// Type: Server; Service: EventServiceClient; COMMON
        case ERR_NOT_CONNECTED_TO_EVENT_SERVICE = "ERR_NOT_CONNECTED_TO_EVENT_SERVICE"
        
        /// Type: Server; Service: EventServiceClient; COMMON
        case ERR_INVALID_TOPIC = "ERR_INVALID_TOPIC"
        
        /// Type: Server; Service: EventServiceClient; COMMON
        case ERR_ALREADY_SUBSCRIBED = "ERR_ALREADY_SUBSCRIBED"
        
        /// Type: Server; Service: EventServiceClient; COMMON
        case ERR_SUBSCRIPTION_ID_NOT_FOUND = "ERR_SUBSCRIPTION_ID_NOT_FOUND"
        
        /// Type: Server; Service: EventServiceClient; COMMON
        case ERR_NOTIFICATION_CONTENT_IS_REQUIRED = "ERR_NOTIFICATION_CONTENT_IS_REQUIRED"
        
        /// Type: Server; Service: EventServiceClient; COMMON
        case ERR_NO_EVENT_SERVICE_FOUND = "ERR_NO_EVENT_SERVICE_FOUND"
        
        /// Type: Client; Service: AuthApi; Specific
        case ERR_AUTH_NO_VERIFIED = "ERR_AUTH_NO_VERIFIED"
        
        /// Type: Client; Service: AuthApi; Specific
        case ERR_AUTH_ALREADY_VERIFIED = "ERR_AUTH_ALREADY_VERIFIED"
        
        /// Type: Client; Service: AuthApi; Specific
        case ERR_AUTH_TOKEN_NOT_VALID = "ERR_AUTH_TOKEN_NOT_VALID"
        
        /// Type: Client; Service: AuthApi; Specific
        case ERR_AUTH_CODE_NOT_VALID = "ERR_AUTH_CODE_NOT_VALID"
        
        /// Type: Client; Service: WinningApi; Specific
        case ERR_COMPONENT_ALREADY_USED = "ERR_COMPONENT_ALREADY_USED"
        
        /// Type: Client; Service: WinningApi; Specific
        case ERR_COMPONENT_NOT_AVAILABLE = "ERR_COMPONENT_NOT_AVAILABLE"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_QUESTION_ALREADY_ANSWERED  = "ERR_GAME_QUESTION_ALREADY_ANSWERED "
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_UNEXPECTED_GAME_QUESTION_ANSWERED = "ERR_UNEXPECTED_GAME_QUESTION_ANSWERED"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_NO_LONGER_AVAILABLE  = "ERR_GAME_NO_LONGER_AVAILABLE "
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_REQUIRE_ENTRY_FEE  = "ERR_GAME_REQUIRE_ENTRY_FEE "
        
        /// Type: Auth; Service: All; COMMON
        case ERR_INSUFFICIENT_RIGHTS  = "ERR_INSUFFICIENT_RIGHTS "
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_ALREADY_STARTED  = "ERR_GAME_ALREADY_STARTED "
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_ALREADY_STARTED_BY_USER  = "ERR_GAME_ALREADY_STARTED_BY_USER "
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_FLOW_VIOLATION = "ERR_GAME_FLOW_VIOLATION"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_CANCELLED = "ERR_GAME_CANCELLED"
        
        /// Type: Client; Service: GameApi; Specific
        case ERR_GAME_PARTICIPANT_COUNT_EXCEEDED = "ERR_GAME_PARTICIPANT_COUNT_EXCEEDED"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_INSUFFICIENT_FUNDS = "ERR_INSUFFICIENT_FUNDS"
        
        /// Type: Client; Service: PaymentApi; Specific
        case NO_EXCHANGE_RATE = "NO_EXCHANGE_RATE"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_MONEY_ACCOUNT_NOT_INITIALIZED = "ERR_MONEY_ACCOUNT_NOT_INITIALIZED"
        
        /// Type: Client; Service: ResultApi; Specific
        case ERR_RESULTS_NOT_RELEASED = "ERR_RESULTS_NOT_RELEASED"
        
        /// Type: Client; Service: TombolaApi; Specific
        case ERR_TOMBOLA_IS_ACTIVATED = "ERR_TOMBOLA_IS_ACTIVATED"
        
        /// Type: Client; Service: TombolaApi; Specific
        case ERR_TOMBOLA_IS_DEACTIVATED = "ERR_TOMBOLA_IS_DEACTIVATED"
        
        /// Type: Client; Service: TombolaApi; Specific
        case ERR_TOMBOLA_IS_ARCHIVED = "ERR_TOMBOLA_IS_ARCHIVED"
        
        /// Type: Client; Service: TombolaApi; Specific
        case ERR_TOMBOLA_PUBLISHING_NO_TOMBOLA_FOUND = "ERR_TOMBOLA_PUBLISHING_NO_TOMBOLA_FOUND"
        
        /// Type: Client; Service: PromocodeApi; Specific
        case ERR_PROMOCODE_CAMPAIGN_IS_ACTIVATED = "ERR_PROMOCODE_CAMPAIGN_IS_ACTIVATED"
        
        /// Type: Client; Service: PromocodeApi; Specific
        case ERR_PROMOCODE_CAMPAIGN_IS_DEACTIVATED = "ERR_PROMOCODE_CAMPAIGN_IS_DEACTIVATED"
        
        /// Type: Client; Service: PromocodeApi; Specific
        case ERR_PROMOCODE_CAMPAIGN_IS_ARCHIVED = "ERR_PROMOCODE_CAMPAIGN_IS_ARCHIVED"
        
        /// Type: Client; Service: PromocodeApi; Specific
        case ERR_PROMOCODE_CAMPAIGN_PUBLISHING_NO_PROMOCODE_CAPMAIGN_FOUND = "ERR_PROMOCODE_CAMPAIGN_PUBLISHING_NO_PROMOCODE_CAPMAIGN_FOUND"
        
        /// Type: Client; Service: PromocodeApi; Specific
        case ERR_PROMOCODE_CAMPAIGN_CONTAINS_PROMOCODE = "ERR_PROMOCODE_CAMPAIGN_CONTAINS_PROMOCODE"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_INVALID_VALUE = "ERR_INVALID_VALUE"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_REQUIRED_PROPERTY_MISSING = "ERR_REQUIRED_PROPERTY_MISSING"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_ACCOUNT_NOT_FOUND = "ERR_ACCOUNT_NOT_FOUND"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_ACCOUNT_STATE_NOT_CHANGEABLE = "ERR_ACCOUNT_STATE_NOT_CHANGEABLE"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_ACCOUNT_NOT_INTERNAL = "ERR_ACCOUNT_NOT_INTERNAL"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_ACCOUNT_ONLY_FOR_TRANSFER = "ERR_ACCOUNT_ONLY_FOR_TRANSFER"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_ACCOUNT_IS_CREDIT_ONLY = "ERR_ACCOUNT_IS_CREDIT_ONLY"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_ACCOUNTTRANSACTION_NOT_FOUND = "ERR_ACCOUNTTRANSACTION_NOT_FOUND"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_USER_NOT_FOUND = "ERR_USER_NOT_FOUND"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_USER_DISABLED = "ERR_USER_DISABLED"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_USER_NOT_IDENTIFIED = "ERR_USER_NOT_IDENTIFIED"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_USER_IS_UNDERAGED = "ERR_USER_IS_UNDERAGED"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_IDENTIFICATION_NOT_FOUND = "ERR_IDENTIFICATION_NOT_FOUND"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_IDENTIFICATION_ALREADY_PROCESSED = "ERR_IDENTIFICATION_ALREADY_PROCESSED"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_IDENTIFICATION_NOT_POSSIBLE = "ERR_IDENTIFICATION_NOT_POSSIBLE"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_PAYMENTTRANSACTION_NOT_FOUND = "ERR_PAYMENTTRANSACTION_NOT_FOUND"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_CASHOUT_LIMIT_REACHED_FOR_IDENTIFICATION_TYPE = "ERR_CASHOUT_LIMIT_REACHED_FOR_IDENTIFICATION_TYPE"
        
        /// Type: Client; Service: PaymentApi; Specific
        case ERR_CASHOUT_LIMIT_NOT_SET = "ERR_CASHOUT_LIMIT_NOT_SET"

        /// Type: Cleint;
        case ERR_INCORECT_EMAIL = "ERR_INCORECT_EMAIL"
    
        case ERR_EMPTY_PASSWORD = "ERR_EMPTY_PASSWORD"
    }
    
    
    
    /// Return message to corresponding error case
    ///
    /// - Parameter error: which error to print
    static func handle(error: ErrorsEnum) -> String {
        
        let defaultValue = "Error occured"
        
        switch error {
        case .ERR_FATAL_ERROR:
            // print localized strings
            return ""
        case .ERR_AUTH_SECRET_WRONG:
            return NSLocalizedString("Supplied login is not recognized", comment: "")
        case .ERR_AUTH_NO_REGISTERED:
            return NSLocalizedString("You are not registered", comment: "")
        case .ERR_AUTH_ALREADY_REGISTERED:
            return NSLocalizedString("This email is already in use.\n Please register with other email account.", comment: "")
        case .ERR_SESSION_STORE_INIT:
             return ""
        case .ERR_SESSION_STORE_DESTROY:
             return ""
        case .ERR_CONNECTION_ERROR:
             return ""
        case .ERR_SHUTTING_DOWN:
             return ""
        case .ERR_VALIDATION_FAILED:
             return NSLocalizedString("You don't have enough rights to access this feature", comment: "")
        case .ERR_ENTRY_ALREADY_EXISTS:
             return ""
        case .ERR_ENTRY_NOT_FOUND:
             return ""
        case .ERR_INCORECT_EMAIL:
            return NSLocalizedString("Pogrešna email adresa", comment: "")
        case .ERR_EMPTY_PASSWORD:
            return NSLocalizedString("Niste uneli šifru", comment: "")
        default:
             return defaultValue
        }
    }
}
